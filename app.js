const express = require('express')
const path = require('path')
const app = express()
const cors = require('cors')
const logger = require('morgan')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const uriDatabase = 'mongodb://bvglzs:bvglzs10@ds017165.mlab.com:17165/bostonprojectdb'

mongoose.connect(uriDatabase).then(
  () => { console.log('Database connection is successful') },
  err => { console.log('Error when connecting to the database' + err) });

var config = require('./config');
var indexRouter = require('./routes/index');
var loginRouter = require('./routes/login');
var carsRouter = require('./routes/car');
var driversRouter = require('./routes/driver');
var sendemailRouter = require('./routes/email');
var recoverRouter = require('./routes/recover');

app.set('port', process.env.PORT || 4000)

app.use(cors());
app.use(logger('dev'))
app.use(bodyParser.json());

app.set('superSecret', config.secret);

app.use('/login', loginRouter);
app.use('/cars', carsRouter);
app.use('/drivers', driversRouter);
app.use('/forgot', sendemailRouter);
app.use('/recover', recoverRouter);

app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.listen(app.get('port'), () => {
  console.log(`Servicer running on http://localhost:${app.get('port')}`)
});

module.exports = app;
