var express = require('express');
var jwt = require('jsonwebtoken');
var config = require('../config')
var Login = require('../models/Login');
var router = express.Router();

router.route('/register/')
  .post(function (req, res) {
    console.log(req.body)
    var login = new Login();
    Login.findOne({ "email": req.body.email }, function (err, user_data) {
      if (err) {
        console.log(err)
      }
      if (user_data) {
        return res.json({
          status: 409,
          message: "User already exist"
        });
      }
      console.log(req.body, 44)
      login.password = req.body.password;
      login.account_type = req.body.account_type;
      login.email = req.body.email;
      login.name = req.body.name;
      console.log(login, 49)
      login.save(function (err, login_data) {
        if (err)
          return res.status(400).send(err);
        res.json({
          status: 200,
          message: 'You have succesfully registered.'
        });
      });
    });
  });

router.route('/login')
  .post(function (req, res) {
    Login.findOne({ "email": req.body.email, "password": req.body.password }, function (err, user_data) {
      if (err || !user_data) {
        return res.status(401).json({
          status: 401,
          message: "Invalid username and password.",
        });
      } else {
        const payload = {
          email: user_data.email,
          account_type: user_data.account_type
        };
        var token = jwt.sign(payload, config.secret, {
          expiresIn: 60 * 60 * 24 // expires in 24 hours
        });
        res.status(200).json({
          message: "You have succesfully loggedin.",
          token: token
        });
      }
    });
  });


// router.use(function (req, res, next) {
//   var token = req.body.token || req.query.token || req.headers['x-access-token'];
//   if (token) {
//     jwt.verify(token, config.secret, function (err, decoded) {
//       if (err) {
//         return res.json({ status: 403, success: false, message: 'Failed to authenticate token.' });
//       } else {
//         req.decoded = decoded;
//         next();
//       }
//     });
//   } else {
//     return res.json({
//       status: 403,
//       success: false,
//       message: 'No token provided.'
//     });
//   }
// });

router.route('/result')
  .get(function (req, res) {
    Login.find(function (err, logins) {
      if (err)
        res.send(err);

      res.json(logins);
    });
  });

router.route('/user')
  .post(function (req, res) {
    console.log(req.body.email)
    var login = new Login();
    Login.findOne({ "email": req.body.email }, function (err, user_data) {
      if (err || !user_data) {
        return res.status(401).json({
          status: 401,
          message: "Invalid email.",
        });
      } else {
        console.log(user_data)
        const payload = {
          email: user_data.email,
          account_type: user_data.account_type,
          name: user_data.name
        };
        console.log(payload)
        var token = jwt.sign(payload, config.secret, {
          expiresIn: 60 * 60 * 24 // expires in 24 hours
        });
        console.log(token)
        return res.status(200).json({
          message: "You have succesfully searched.",
          token: token
        });
      }
    });
  });

module.exports = router;
