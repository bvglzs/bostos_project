var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var DriverSchema = new Schema({
  name: {
    type: String,
    required: 'Please provide the name'
  },
  birthdate: {
    type: String,
    required: 'Please provide the birthdate'
  }
});

module.exports = mongoose.model('Driver', DriverSchema);
