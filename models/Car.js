var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var CarSchema = new Schema({
  plate: {
    type: String,
    required: 'Please provide the plate'
  },
  model: {
    type: String,
    required: 'Please provide the model'
  }
});

module.exports = mongoose.model('Car', CarSchema);
