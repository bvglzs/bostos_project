var express = require('express');

var Driver = require('../models/Driver');
var router = express.Router();

router.route('/register/')
  .post(function (req, res) {
    console.log(req.body)
    var driver = new Driver();
    Driver.findOne({ "name": req.body.name }, function (err, user_data) {
      if (err) {
        console.log(err)
      }
      if (user_data) {
        return res.json({
          status: 409,
          message: "Driver already exist"
        });
      }
      console.log(req.body, 44)
      driver.name = req.body.name;
      driver.birthdate = req.body.birthdate;
      console.log(Driver, 49)
      driver.save(function (err, Driver_data) {
        if (err)
          return res.status(400).send(err);
        res.json({
          status: 200,
          message: 'You have succesfully registered.'
        });
      });
    });
  });

router.route('/')
  .get(function (req, res) {
    Driver.find(function (err, drivers) {
      if (err)
        res.send(err);

      res.json(drivers);
    });
  });

module.exports = router;
