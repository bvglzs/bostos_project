var express = require('express');
var nodemailer = require('nodemailer');
var jwt = require('jwt-simple');
var config = require('../config');
var router = express.Router();
const user = 'brandongallego89@gmail.com';
const pass = 'bvglzslds10';

router.route("/").post(function (req, res)  {
  let tokenGenerated = req.body.token;
  const tokenDecoded = jwt.decode(tokenGenerated, config.secret);
  console.log(tokenDecoded)
  sendMail(tokenDecoded, tokenGenerated, info => {
    console.log(`The mail has beed send 😃 and the id is ${info.messageId}`);
    res.send(info);
  });
});

async function sendMail(tokenDecoded, tokenGenerated, callback) {
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: user,
      pass: pass
    }
  });

  let mailOptions = {
    from: '"Boston Project"<brandonvg89@gmail.com>', // sender address
    to: tokenDecoded.email, // list of receivers
    subject: "Recover your password", // Subject line
    html: `<h1>Hi ${tokenDecoded.name}</h1><br>
    <a href="http://localhost:3000/recover?token=${tokenGenerated}">Click here for recover your password</a>`
  };

  // send mail with defined transport object
  let info = await transporter.sendMail(mailOptions);

  callback(info);
}

module.exports = router;
