import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home.vue'
import Login from '@/components/Login.vue'
import Register from '@/components/Register.vue'
import Driver from '@/components/Driver.vue'
import Forgot from '@/components/Forgot.vue'
import Recover from '@/components/Recover.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },{
      path: '/home',
      name: 'home',
      component: Home
    }, {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/driver',
      name: 'Driver',
      component: Driver
    },
    {
      path: '/forgot',
      name: 'Forgot',
      component: Forgot
    },
    {
      path: '/recover',
      name: 'Recover',
      component: Recover
    }
  ]
})
